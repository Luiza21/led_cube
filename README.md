# RGB LED Cube 8x8x8

The purpose of this project is to create 8x8x8 RGB LED Cube.

Tasks to complete:
- LED Cube construction preparation,
- PCB schematic design and layout:
	- PCB to control LED's cathodes with shift registers,
	- PCB with microcontroller,
- Software: 
	- STM32 bare metal programming
	- Android/PC app to control the cube



Inspiration to start this project: https://www.instructables.com/8x8x8-RGB-LED-Cube-1/ (access date: 09.01.2023)
