#ifndef __SPI_H
#define __SPI_H

#include "stm32l475xx.h"
#include "GPIO.h"

#define SPI1_ID     0U
#define SPI2_ID     1U
#define SPI3_ID     2U

 /*         SPI2

| PORT | PIN | Name | AF  |
---------------------------
|    B |   9 |   CS |   5 |
|    B |  10 |  CLK |   5 |
|    B |  15 | MOSI |   5 |
*/
#define SPI2_CS     PIN('B', 9)
#define SPI2_CLK    PIN('B', 10)
#define SPI2_MOSI   PIN('B', 15)

#define AF 5


/* --------------------------------------------------------------------------------------------- */
/* ------------------------------------ Public functions --------------------------------------- */
/* --------------------------------------------------------------------------------------------- */

void Spi_Init(uint8_t spiInstance);
void Spi_WriteByte(uint8_t spiInstance, uint8_t data);

#endif /* __SPI_H */