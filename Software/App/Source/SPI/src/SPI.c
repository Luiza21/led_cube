#include "SPI.h"

static void spi_configureGpio(uint8_t spiInstance);
static void spi_enableClk(uint8_t spiInstance);
static SPI_TypeDef* spi_getInstanceAddress(uint8_t spiInstance);

/* --------------------------------------------------------------------------------------------- */
/* ------------------------------------ Public functions --------------------------------------- */
/* --------------------------------------------------------------------------------------------- */

/**
 * @brief Initializes SPI interface
 * @param spiInstance SPI number
*/
void Spi_Init(uint8_t spiInstance)
{
    SPI_TypeDef* hspi = spi_getInstanceAddress(spiInstance);

    spi_enableClk(spiInstance);
    spi_configureGpio(spiInstance);

    // 2.a) Configure the serial clock baud rate using the BR[2:0] bits
    // Main clock: 80MHz
    hspi->CR1 |= (SPI_CR1_BR_0); // fclk/8
    // 2.b) Configure CPOL and CPHA
    hspi->CR1 &= ~(SPI_CR1_CPHA_Msk & SPI_CR1_CPHA); // CPHA = 0
    hspi->CR1 &= ~(SPI_CR1_CPOL_Msk & SPI_CR1_CPOL); // CPOL = 0
    // 2.c) Select simplex mode
    hspi->CR1 &= ~(SPI_CR1_RXONLY_Msk & SPI_CR1_RXONLY);
    // 2.d) Define frame format
    hspi->CR1 &= ~(SPI_CR1_LSBFIRST_Msk & SPI_CR1_LSBFIRST); // Data tx with the MSB first
    // 2.e) Disable hardware CRC calculation
    hspi->CR1 &= ~(SPI_CR1_CRCEN_Msk & SPI_CR1_CRCEN);
    // 2.f) Configure SSM and SSI
    hspi->CR1 |= (SPI_CR1_SSM_Msk & SPI_CR1_SSM); // Hardware slave management
    // SSI configuration skipped - valid only when SSM is set
    // 2.g) Configure the MSTR bit
    hspi->CR1 |= (SPI_CR1_MSTR_Msk & SPI_CR1_MSTR); // Master configuration
    // 3.a) Configure the DS[3:0] bits to select the data length for the transfer
    hspi->CR2 |= (SPI_CR2_DS_Msk & (SPI_CR2_DS_2 | SPI_CR2_DS_1 |SPI_CR2_DS_0));
    // 3.b) Configure SSOE
    hspi->CR2 |= (SPI_CR2_SSOE_Msk & SPI_CR2_SSOE); // Tx buffer DMA disabled
    // 3.c) Set the FRF bit if needed
    hspi->CR2 &= ~(SPI_CR2_FRF_Msk & SPI_CR2_FRF); // SPI Motorola mode
    // 3.d) Configure NSS pulse
    hspi->CR2 |= (SPI_CR2_NSSP_Msk & SPI_CR2_NSSP);
    // TODO - configure DMA
    // 3.e) Configure fifo threshold
    // 3.f) Initialize DMA
    // 4. Configure the CRC polynomial if needed - not needed
    // 5. Configure DMA
    // Enable SPI
    hspi->CR1 |= (SPI_CR1_SPE_Msk & SPI_CR1_SPE);
}

void Spi_WriteByte(uint8_t spiInstance, uint8_t data)
{
    SPI_TypeDef *hspi = ((void *)0);

    hspi = spi_getInstanceAddress(spiInstance);
    *(uint8_t*)&hspi->DR = data; // Data packing - only 8 bits have to be sent

    while (hspi->SR & SPI_SR_BSY) spin(1); // Wait for transmission to complete
}

/* --------------------------------------------------------------------------------------------- */
/* ------------------------------------ Private functions -------------------------------------- */
/* --------------------------------------------------------------------------------------------- */

/**
 * @brief Configures SPI GPIO pins: MISO, MOSI, CLK and CS
 * @param spiInstance SPI number
*/
static void spi_configureGpio(uint8_t spiInstance)
{
    switch (spiInstance)
    {
        case SPI1_ID:
            // Add if needed
            break;
        case SPI2_ID:
            Gpio_Init(SPI2_CS,   GPIO_MODE_OUTPUT, GPIO_OTYPE_PUSH_PULL, GPIO_SPEED_INSANE, GPIO_PULL_NONE, AF);
            Gpio_Init(SPI2_CLK,  GPIO_MODE_AF, GPIO_OTYPE_PUSH_PULL, GPIO_SPEED_INSANE, GPIO_PULL_NONE, AF);
            Gpio_Init(SPI2_MOSI, GPIO_MODE_AF, GPIO_OTYPE_PUSH_PULL, GPIO_SPEED_INSANE, GPIO_PULL_NONE, AF);
            break;
        case SPI3_ID:
            // Add if needed
            break;
        default:
            break;
    }
}

/**
 * @brief Enables SPI peripherial clock
 * @param spiInstance SPI number
*/
static void spi_enableClk(uint8_t spiInstance)
{
    switch (spiInstance)
    {
        case SPI1_ID:
            // Enable SPI1 peripherial clock
            RCC->APB2ENR |= RCC_APB2ENR_SPI1EN_Msk;
            break;
        case SPI2_ID:
            // Enable SPI2 peripherial clock
            RCC->APB1ENR1 |= RCC_APB1ENR1_SPI2EN_Msk;
            break;
        case SPI3_ID:
            // Enable SPI3 peripherial clock
            RCC->APB1ENR1 |= RCC_APB1ENR1_SPI3EN_Msk;
            break;
        default:
            break;
    }
}

/**
 * @brief  Gets address of SPI instance
 * @param  spiInstance SPI number
 * @retval SPI interface address
*/
static SPI_TypeDef* spi_getInstanceAddress(uint8_t spiInstance)
{
    SPI_TypeDef* spiAddr = ((void *)0);

    switch (spiInstance)
    {
        case SPI1_ID:
            spiAddr = SPI1;
            break;
        case SPI2_ID:
            spiAddr = SPI2;
            break;
        case SPI3_ID:
            spiAddr = SPI3;
            break;
        default:
            break;
    }

    return spiAddr;
}
