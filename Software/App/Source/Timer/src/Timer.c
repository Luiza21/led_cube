#include "Timer.h"

#define LED_MSK 0xFE01

void Timer_Init(void)
{
    // TIM1

    // Disable timer
    TIM7->CR1 &= ~(TIM_CR1_CEN);

    // Clock enable
    RCC->APB1ENR1 |= RCC_APB1ENR1_TIM7EN;

    // Enable NVIC
    NVIC_SetPriority(TIM7_IRQn, 0x03);
    NVIC_EnableIRQ(TIM7_IRQn);

    RCC->APB1RSTR1 |= RCC_APB1RSTR1_TIM6RST;
    RCC->APB1RSTR1 &= ~(RCC_APB1RSTR1_TIM6RST);

    // Enable interrupt
    TIM7->DIER |= TIM_DIER_UIE;

    // Update event to reset the timer and apply settings
    TIM7->EGR |= TIM_EGR_UG;

    // Prescaler
    TIM7->PSC = 0x15U; // Presc = 2

    // ARR
    TIM7->ARR = 0x00FF;

    // Enable timer
    TIM7->CR1 |= TIM_CR1_CEN;
}

void TIM7_IRQHandler(void)
{
    static uint16_t layerNo = 2U;

    if (TIM7->SR & TIM_SR_UIF)
    {
        TIM7->SR &= ~(TIM_SR_UIF);

        layerNo *= 2U;
        if (layerNo % 512 == 0)
        {
            layerNo = 2U;
        }

        GPIOA->ODR = (LED_MSK | layerNo);
    }
}