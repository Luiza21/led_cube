#ifndef __SYSCLK_H
#define __SYSCLK_H

#include "stm32l475xx.h"

#define FLASH_LATENCY 0x07 //Zero wait state

void SysClk_Init(void);

#endif /* __SYSCLK_H */