#include "SysClk.h"

void SysClk_Init(void)
{
    // Input frequency = 8 Mz from external oscillator
    // Target frequency = 80 Mhz

    // Enable power interface controller
    RCC->APB1ENR1 |= RCC_APB1ENR1_PWREN;

    // Check vlotage scaling
    PWR->CR1 |= PWR_CR1_VOS_0; // Set voltage scaling range 1 - SYSCLK freq. up to 80 MHz

    // Configure flash controller for 1.2V power supply and 80 MHz
    FLASH->ACR &= ~(FLASH_LATENCY);

    // Disable PLL before configuration by setting PLLON to 0
    RCC->CR &= ~(RCC_CR_PLLON);

    // Wait until PLLRDY is cleared. The PLL is now fully stopped.
    while(RCC->CR & RCC_CR_PLLRDY) {asm("NOP");};

    // Modify desired parameters
    // Output freq = (input * PLLN)/(PLLM * PLLR)
    // Input = 8 Mhz
    // PLLN = 20
    // PLLM = 1
    // PLLR = 2

    // Modify PLLM factor
    RCC->PLLCFGR &= ~(RCC_PLLCFGR_PLLM_0 | RCC_PLLCFGR_PLLM_1 | RCC_PLLCFGR_PLLM_2); // PLLM = 1

    // Modify PLLN factor
    RCC->PLLCFGR &= ~(RCC_PLLCFGR_PLLN); // Reset PLLN value
    RCC->PLLCFGR |= RCC_PLLCFGR_PLLN_4 | RCC_PLLCFGR_PLLN_2; // PLLN = 20

    // Modify PLLR factor
    RCC->PLLCFGR |= RCC_PLLCFGR_PLLR_0; // PLLR = 2

    // Turn HSE on
    RCC->CR |= RCC_CR_HSEON;

    // Wait for HSE to be ready
    while (!(RCC->CR & RCC_CR_HSERDY)) { asm("NOP"); };

    // Select source of PLL clock
    RCC->PLLCFGR |= RCC_PLLCFGR_PLLSRC_HSE;

    // Enable PLL after configuration by setting PLLON to 1
    RCC->CR |= RCC_CR_PLLON;

    // Enable PLLREN
    RCC->PLLCFGR |= RCC_PLLCFGR_PLLREN;

    // Set APB2 prescaler
    RCC-> CFGR &= ~(RCC_CFGR_PPRE2); // HCLK not divided

    // Set APB1 prescaler
    RCC-> CFGR &= ~(RCC_CFGR_PPRE1); // HCLK not divided

    // Set HPRE prescaler
    RCC-> CFGR &= ~(RCC_CFGR_HPRE); // SYSCLK not divided

    // Select PLL as sysclk source
    RCC->CFGR |= RCC_CFGR_SW_PLL;

    // Wait till PLL is selected as sysclk source
    while(!(RCC->CFGR & RCC_CFGR_SWS_PLL)) { asm("NOP"); };
}