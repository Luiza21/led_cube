#ifndef __SHIFTREG_H
#define __SHIFTREG_H

#include "stm32l475xx.h"
#include "GPIO.h"
#include "SPI.h"

#define SHIFT_REG_NRST   PIN('B', 11)
#define SHIFT_REG_LC     PIN('B', 12)

/* --------------------------------------------------------------------------------------------- */
/* ------------------------------------ Public functions --------------------------------------- */
/* --------------------------------------------------------------------------------------------- */

void ShiftReg_Init(void);
void ShiftReg_SendRGB(uint8_t red, uint8_t green, uint8_t blue);

#endif /* __SHIFTREG_H */