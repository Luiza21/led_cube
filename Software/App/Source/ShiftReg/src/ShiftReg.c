#include "ShiftReg.h"

/* --------------------------------------------------------------------------------------------- */
/* ------------------------------------ Public functions --------------------------------------- */
/* --------------------------------------------------------------------------------------------- */

void ShiftReg_Init(void)
{
    Gpio_Output(SHIFT_REG_NRST);
    Gpio_Output(SHIFT_REG_LC);
    Gpio_Set(SHIFT_REG_NRST);
}

void ShiftReg_SendRGB(uint8_t red, uint8_t green, uint8_t blue)
{
    Gpio_Write(SHIFT_REG_LC, 0);

    for (int i = 0; i < 8; i++)
    {
        Spi_WriteByte(SPI2_ID, red);
        Spi_WriteByte(SPI2_ID, green);
        Spi_WriteByte(SPI2_ID, blue);
    }
    Gpio_Write(SHIFT_REG_LC, 1);
}
