#include "ShowCont.h"

struct colors
{
    uint8_t red;
    uint8_t green;
    uint8_t blue;
};

const uint16_t cathodesPins[NO_OF_LAYERS] = {LAYER_1, LAYER_2, LAYER_3, LAYER_4,
                                             LAYER_5, LAYER_6, LAYER_7, LAYER_8};

static void showCont_sendLayer(uint8_t color, uint16_t layerID);
static void showCont_sendFrame(uint8_t frameNumber);
static void showCont_sendFrame1(uint8_t frameNumber);
static void showCont_sendRGB(uint8_t red, uint8_t green, uint8_t blue);

/* --------------------------------------------------------------------------------------------- */
/* ------------------------------------ Public functions --------------------------------------- */
/* --------------------------------------------------------------------------------------------- */

void ShowCont_init(void)
{
    uint8_t layerNo = 0U;

    for (layerNo = 0; layerNo < NO_OF_LAYERS; layerNo++)
    {
        Gpio_Output(cathodesPins[layerNo]);
        Gpio_Write(cathodesPins[layerNo], 1);
    }
}

void ShowCont_runSequence(uint8_t seqNumber)
{
    showCont_sendFrame(seqNumber);
}

void ShowCont_setColor(color colorNumber)
{
    switch(colorNumber)
    {
        case TURN_OFF:
            showCont_sendRGB(0x00, 0x00, 0x00);
            break;
        case RED:
            showCont_sendRGB(0xFF, 0, 0);
            break;
        case PINK:
            showCont_sendRGB(0xFF, 0, 0xFF);
            break;
        case BLUE:
            showCont_sendRGB(0, 0, 0xFF);
            break;
        case TURQUOISE:
            showCont_sendRGB(0, 0xFF, 0xFF);
            break;
        case GREEN:
            showCont_sendRGB(0, 0xFF, 0);
            break;
        case YELLOW:
            showCont_sendRGB(0xFF, 0xFF, 0);
            break;
        default:
            break;
    }
}

/* --------------------------------------------------------------------------------------------- */
/* ------------------------------------ Private functions -------------------------------------- */
/* --------------------------------------------------------------------------------------------- */

static void showCont_sendFrame(uint8_t seqNumber)
{
 
    
}

void showCont_sendLayer(uint8_t color, uint16_t layerID)
{
    ShowCont_setColor(color);
    GPIOA->ODR = layerID;
}



static void showCont_sendRGB(uint8_t red, uint8_t green, uint8_t blue)
{
    ShiftReg_SendRGB(red, green, blue);
}