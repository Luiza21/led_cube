#ifndef __SHOWCONT_H
#define __SHOWCONT_H

#include "stm32l475xx.h"
#include "Gpio.h"
#include "SPI.h"
#include "ShiftReg.h"

#define NO_OF_LAYERS 8

#define LAYER_1 PIN('A', 1)
#define LAYER_2 PIN('A', 2)
#define LAYER_3 PIN('A', 3)
#define LAYER_4 PIN('A', 4)
#define LAYER_5 PIN('A', 5)
#define LAYER_6 PIN('A', 6)
#define LAYER_7 PIN('A', 7)
#define LAYER_8 PIN('A', 8)

typedef enum
{
    COLOR_MIN,
    TURN_OFF = 0,
    RED,
    PINK,
    BLUE,
    TURQUOISE,
    GREEN,
    YELLOW,
    COLOR_MAX = YELLOW
    // TODO - add new colors - think how to implement it: multiplexing
} color;


/* --------------------------------------------------------------------------------------------- */
/* ------------------------------------ Public functions --------------------------------------- */
/* --------------------------------------------------------------------------------------------- */

void ShowCont_init(void);
void ShowCont_runSequence(uint8_t seqNumber);
void ShowCont_setColor(color colorNumber);

#endif /* __SHOWCONT_H */