#include "GPIO.h"

#define GPIO(N) ((GPIO_TypeDef *) (GPIOA_BASE + 0x400 * (N)))

static GPIO_TypeDef *gpio_Bank(uint16_t pin);

void spin(volatile uint32_t count)
{
    while (count--) (void) 0;
}

/* --------------------------------------------------------------------------------------------- */
/* ------------------------------------ Public functions --------------------------------------- */
/* --------------------------------------------------------------------------------------------- */

void Gpio_Set(uint16_t pin)
{
    Gpio_Write(pin, 1);
}

void Gpio_Reset(uint16_t pin)
{
    Gpio_Write(pin, 1);
}

void Gpio_Toggle(uint16_t pin)
{
    GPIO_TypeDef *gpio = gpio_Bank(pin);
    uint32_t mask = BIT(PINNO(pin));
    gpio->BSRR = mask << (gpio->ODR & mask ? 16 : 0);
}

int Gpio_Read(uint16_t pin)
{
    return gpio_Bank(pin)->IDR & BIT(PINNO(pin)) ? 1 : 0;
}

void Gpio_Write(uint16_t pin, bool val)
{
    GPIO_TypeDef *gpio = gpio_Bank(pin);
    gpio->BSRR = BIT(PINNO(pin)) << (val ? 0 : 16);
}

void Gpio_Init(uint16_t pin, uint8_t mode, uint8_t type,
                             uint8_t speed, uint8_t pull, uint8_t af)
{
    GPIO_TypeDef *gpio = gpio_Bank(pin);
    uint8_t n = (uint8_t) (PINNO(pin));
    RCC->AHB2ENR |= BIT(PINBANK(pin));  // Enable GPIO clock
    SETBITS(gpio->MODER, 3UL << (n * 2), ((uint32_t) mode) << (n * 2));
    SETBITS(gpio->OTYPER, 1UL << n, ((uint32_t) type) << n);
    SETBITS(gpio->OSPEEDR, 3UL << (n * 2), ((uint32_t) speed) << (n * 2));
    SETBITS(gpio->PUPDR, 3UL << (n * 2), ((uint32_t) pull) << (n * 2));
    SETBITS(gpio->AFR[n >> 3], 15UL << ((n & 7) * 4),
           ((uint32_t) af) << ((n & 7) * 4));
}

void Gpio_Input(uint16_t pin)
{
    Gpio_Init(pin, GPIO_MODE_INPUT, GPIO_OTYPE_PUSH_PULL, GPIO_SPEED_HIGH, GPIO_PULL_NONE, 0);
}

void Gpio_Output(uint16_t pin)
{
    Gpio_Init(pin, GPIO_MODE_OUTPUT, GPIO_OTYPE_PUSH_PULL, GPIO_SPEED_HIGH, GPIO_PULL_NONE, 0);
}

/* --------------------------------------------------------------------------------------------- */
/* ------------------------------------ Private functions -------------------------------------- */
/* --------------------------------------------------------------------------------------------- */

static GPIO_TypeDef *gpio_Bank(uint16_t pin)
{
    return GPIO(PINBANK(pin));
}
