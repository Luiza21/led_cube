#ifndef __GPIO_H
#define __GPIO_H

#include "stm32l475xx.h"

enum
{
    GPIO_MODE_INPUT,
    GPIO_MODE_OUTPUT,
    GPIO_MODE_AF,
    GPIO_MODE_ANALOG
};

enum
{
    GPIO_OTYPE_PUSH_PULL,
    GPIO_OTYPE_OPEN_DRAIN
};

enum
{
    GPIO_SPEED_LOW,
    GPIO_SPEED_MEDIUM,
    GPIO_SPEED_HIGH,
    GPIO_SPEED_INSANE
};

enum
{
    GPIO_PULL_NONE,
    GPIO_PULL_UP,
    GPIO_PULL_DOWN
};

#define BIT(x) (1UL << (x))
#define SETBITS(R, CLEARMASK, SETMASK) (R) = ((R) & ~(CLEARMASK)) | (SETMASK)
#define PIN(bank, num) ((((bank) - 'A') << 8) | (num))
#define PINNO(pin) (pin & 255)
#define PINBANK(pin) (pin >> 8)

#ifndef LED_PIN
#define LED_PIN PIN('C', 7)
#endif

#ifndef LED_PIN2
#define LED_PIN2 PIN('C', 8)
#endif

#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>

void spin(volatile uint32_t count);

void Gpio_Set(uint16_t pin);
void Gpio_Reset(uint16_t pin);
void Gpio_Toggle(uint16_t pin);
int  Gpio_Read(uint16_t pin);
void Gpio_Write(uint16_t pin, bool val);
void Gpio_Init(uint16_t pin, uint8_t mode, uint8_t type,
                             uint8_t speed, uint8_t pull, uint8_t af);
void Gpio_Input(uint16_t pin);
void Gpio_Output(uint16_t pin);

#endif /* __GPIO_H */
